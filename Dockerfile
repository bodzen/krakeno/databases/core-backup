FROM postgres:12.3-alpine

COPY key.pub /root/key.pub

RUN apk add gnupg && \
	gpg --import /root/key.pub && \
	mkdir /opt/backup
